#!/bin/bash
source $(dirname "$0")/base.sh
set -eo pipefail

# trap errors as a CI system failure
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

logger "gitlab CI: preparing $MACHINE"

create_rootfs_if_needed () {
  LOCKFILE="/tmp/my.lock"
  lockfile -1 -r 300 -l 600 $LOCKFILE || exit 1
  if [ ! -d $D  ]; then
    logger "creating $ROOTFS using debootstrap"
    # Include git (required by gitlab) and systemd-container
    debootstrap --include=systemd-container,git sid "$ROOTFS"
  fi
  rm -f $LOCKFILE
}

# create_rootfs_if_needed
mkdir -p $OVERLAY
systemd-nspawn --quiet -D "$ROOTFS" \
  --overlay="$ROOTFS:$OVERLAY:/" \
  --bind-ro="$TMPDIR" \
  --machine=$MACHINE \
  apt install --no-install-recommends -y git
